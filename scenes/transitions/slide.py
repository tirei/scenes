# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import random
import time
from abc import abstractmethod

import transition


class SlideTransition(transition.Transition):
    time = 1

    def run(self):
        shape = self.create_shape()
        self.canvas.itemconfig(shape, outline=self.color)
        start = time.monotonic()
        diff = time.monotonic() - start
        while diff < self.time:
            self.canvas.itemconfig(shape, width=self.width * diff * 2 / self.time)
            self.update()
            diff = time.monotonic() - start

    @abstractmethod
    def create_shape(self) -> int:
        raise NotImplementedError()


class HSlide(SlideTransition):
    def create_shape(self):
        side = random.choice((-1, 1))
        return self.canvas.create_rectangle(
            side * self.width / 2,
            -self.height / 2,
            side * self.width / 2,
            self.height / 2,
        )


class VSlide(SlideTransition):
    def create_shape(self):
        side = random.choice((-1, 1))
        return self.canvas.create_rectangle(
            -self.width / 2,
            side * self.height / 2,
            self.width / 2,
            side * self.height / 2,
        )
