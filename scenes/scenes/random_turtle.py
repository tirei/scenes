# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import random

import scene


class RandomTurtle(scene.TurtleScene):
    def run(self):
        min_dist = int(self.height / 32)
        pen = self.pen(0, 0)
        pen.speed(0)
        for i in range(random.randint(16, 256)):
            pen.left(random.randint(-90, 90))
            dist = random.randint(min_dist, min_dist * 8)
            if (abs(pen.xcor()) + dist) * 2 > self.width or (
                abs(pen.ycor()) + dist
            ) * 2 > self.height:
                pen.setheading(pen.towards(0, 0))
            pen.forward(dist)
