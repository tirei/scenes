# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import random

import scene


class Dragon(scene.TurtleScene):
    def run(self):
        # init pen
        self.__pen = self.pen(0, 0, speed=0)
        self.__pen.hideturtle()

        # determine params
        delay = random.randint(0, 2)
        # TODO: better size and depth
        size = random.randint(self.height // 540, self.height // 67)
        depth = random.randint(8, 24 - (size * 1080 // self.height))
        rotation = random.choice((-90, 90))
        self.log.info(
            "dragon params: delay {}, size {}, depth {}, rotation {}".format(
                delay, size, depth, rotation
            )
        )

        # apply params
        self.__pen.screen.delay(delay)
        self.__dragon(size, depth, rotation)

    def __dragon(self, size: int, depth: int, rotation: int):
        if depth == 0:
            # draw line
            self.__pen.forward(size)
        else:
            # replace line by (sub-) dragon
            self.__dragon(size, depth - 1, 90)
            self.__pen.right(rotation)
            self.__dragon(size, depth - 1, -90)
