# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import random
from typing import List, Tuple

import scene


class ConnectedBitField(scene.TurtleScene):
    def run(self):
        self.__width = random.randint(4, 16)
        self.__size = int(self.width / self.__width)
        self.__height = int(self.height / self.__size)
        self.__offset_x = (self.width % self.__size) // 2
        self.__offset_y = (self.height % self.__size) // 2
        self.log.info("field size: {}x{}".format(self.__width, self.__height))

        field = [
            [random.choice((True, False)) for y in range(self.__height)]
            for x in range(self.__width)
        ]

        pen = self.pen(self.coords(0, 0))
        for y in range(self.__height):
            for x in range(self.__width):
                self.log.debug("checking ({}, {})".format(x, y))
                if field[x][y] is not None:
                    self.turn_off(pen)
                    pen.goto(self.coords(x, y))
                    self.draw(pen, field, x, y)

    def draw(self, pen, field: List[List[bool]], x: int, y: int):
        cell = field[x][y]
        self.log.debug("walking ({}, {}) = {}".format(x, y, cell))
        field[x][y] = None
        for ny in range(min(self.__height - 1, y + 1), max(-1, y - 2), -1):
            for nx in range(min(self.__width - 1, x + 1), max(-1, x - 2), -1):
                if nx == x and ny == y:
                    continue
                if field[nx][ny] == cell:
                    self.turn_on(pen, speed=6)
                    self.goto(pen, nx, ny)
                    self.draw(pen, field, nx, ny)
                    pen.speed(0)
                    self.goto(pen, x, y)

    def goto(self, pen, x: int, y: int):
        coords = self.coords(x, y)
        pen.setheading(pen.towards(coords))
        pen.goto(coords)

    def coords(self, x: int, y: int) -> Tuple[int, int]:
        return (
            x * self.__size + (self.__size - self.width) // 2 + self.__offset_x,
            y * self.__size + (self.__size - self.height) // 2 + self.__offset_y,
        )
