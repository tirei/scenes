# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import show_element


class Transition(show_element.ShowElement):
    def __init__(
        self,
        log: "logging.Logger",
        app: "application.Application",
        canvas: "tkinter.Canvas",
        color: str,
    ):
        super().__init__(log, app)
        self.canvas = canvas
        self.color = color
