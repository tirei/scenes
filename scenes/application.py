# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
import inspect
import os
import random
import time
import tkinter as tk
from typing import Iterator, List

import scene
import transition


class Application(object):
    __colors = {
        "#FFFFFF": ["#000000", "#FF0000", "#0000FF"],
        "#000000": ["#FFFFFF", "#FF0000", "#00FF00", "#0000FF"],
        "#FF0000": ["#000000", "#FFFFFF", "#00FF00", "#0000FF"],
        "#00FF00": ["#000000", "#FF0000", "#0000FF"],
        "#0000FF": ["#000000", "#FFFFFF", "#FF0000", "#00FF00"],
    }

    def __init__(
        self, log: "logging.Logger", scene_mods: List[str], transition_mods: List[str]
    ):
        self.__log = log.getChild("app")
        self.__scene_log = log.getChild("scene")
        self.__transition_log = log.getChild("transition")

        if scene_mods:
            self.__scenes = []
            for mod in scene_mods:
                self.__scenes += list(self.__import_mod("scenes", mod, scene.Scene))
        else:
            self.__scenes = list(self.__import_dir("scenes", scene.Scene))
        self.__log.debug("scenes loaded: {}".format(self.__scenes))
        if transition_mods:
            self.__transitions = []
            for mod in transition_mods:
                self.__transitions += list(
                    self.__import_mod("transitions", mod, transition.Transition)
                )
        else:
            self.__transitions = list(
                self.__import_dir("transitions", transition.Transition)
            )
        self.__log.debug("transitions loaded: {}".format(self.__transitions))

        self.__running = True
        self.__bg_color = "#FFFFFF"

        self.__tk = tk.Tk()
        self.__tk.protocol("WM_DELETE_WINDOW", lambda: self.quit())
        self.__tk.bind("<Escape>", lambda *args: self.quit())
        self.__tk.bind("q", lambda *args: self.quit())
        self.__tk.attributes("-fullscreen", True)
        self.__tk.focus_set()
        self.__tk.config(cursor="none")
        self.sleep(0.2)  # acquire full screen size
        self.__log.debug("size: {}x{}".format(self.width, self.height))
        self.__canvas = tk.Canvas(
            self.__tk,
            bg=self.__bg_color,
            width=self.width - 1,
            height=self.height - 1,
            highlightthickness=0,
            borderwidth=0,
        )
        self.__canvas.pack(fill="both")
        self.__tk.bind("<Configure>", lambda *args: self.resize())

    @property
    def width(self) -> int:
        return self.__tk.winfo_width()

    @property
    def height(self) -> int:
        return self.__tk.winfo_height()

    def loop(self):
        self.__log.info("start")
        try:
            while self.__running:
                fg_color = random.choice(self.__colors[self.__bg_color])
                scene_class = random.choice(self.__scenes)
                self.__log.debug(
                    "init {} scene in {} on {}".format(
                        scene_class.__name__, fg_color, self.__bg_color
                    )
                )
                scene = scene_class(
                    self.__scene_log, self, self.__canvas, fg_color, self.__bg_color
                )
                self.__log.info("running scene {}".format(str(scene)))
                scene.run()
                self.__log.info("holding scene")
                self.sleep(1)

                self.__bg_color = fg_color
                transition_class = random.choice(self.__transitions)
                self.__log.debug(
                    "init {} transition to {}".format(
                        transition_class.__name__, self.__bg_color
                    )
                )
                transition = transition_class(
                    self.__transition_log, self, self.__canvas, self.__bg_color
                )
                self.__log.info("running transition {}".format(str(transition)))
                transition.run()
                self.__log.debug("resetting canvas to {}".format(self.__bg_color))
                self.__canvas.config(bg=self.__bg_color)
                self.__canvas.delete("all")
                self.update()
        except Quit:
            self.__quit()

    def resize(self):
        self.__log.info("resize")
        self.__canvas.config(width=self.width, height=self.height)

    def update(self):
        if self.__running:
            self.__tk.update_idletasks()
            self.__tk.update()
        else:
            self.__log.debug("trying to quit")
            raise Quit()

    def sleep(self, seconds: int):
        self.__log.debug("sleep {}s".format(seconds))
        end = time.monotonic() + seconds
        while time.monotonic() < end:
            self.update()

    def quit(self):
        self.__log.info("quitting")
        self.__running = False

    def __quit(self):
        self.__log.debug("destroy Tk")
        self.__tk.destroy()

    def __import_dir(self, directory: str, base_class: type) -> Iterator[type]:
        # load plugins
        for entry in os.scandir(
            os.path.join(os.path.dirname(os.path.abspath(__file__)), directory)
        ):
            name = entry.name
            if name[-3:] == ".py":
                yield from self.__import_mod(directory, name[:-3], base_class)

    def __import_mod(
        self, directory: str, name: str, base_class: type
    ) -> Iterator[type]:
        mod = importlib.import_module(directory + "." + name)
        for item in dir(mod):
            item = getattr(mod, item)
            if (
                isinstance(item, type)
                and issubclass(item, base_class)
                and not inspect.isabstract(item)
            ):
                yield item


class Quit(Exception):
    pass
