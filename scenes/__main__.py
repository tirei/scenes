# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import logging

import application

log = logging.getLogger("scenes")
log.setLevel(logging.DEBUG)
log_stdout = logging.StreamHandler()
log_stdout.setFormatter(logging.Formatter("%(levelname)s:%(name)s: %(message)s"))
log.addHandler(log_stdout)

log.debug("parsing arguments")
parser = argparse.ArgumentParser()
parser.add_argument(
    "-s",
    "--scenes",
    nargs="+",
    metavar="MODULE",
    help="load scenes only from these modules",
)
parser.add_argument(
    "-t",
    "--transitions",
    nargs="+",
    metavar="MODULE",
    help="load transitions only from these modules",
)
args = parser.parse_args()

app = application.Application(log, args.scenes, args.transitions)
app.loop()
