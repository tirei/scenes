# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from abc import ABC, abstractmethod
from typing import Tuple


class ShowElement(ABC):
    def __init__(self, log: "logging.Logger", app: "application.Application"):
        self.log = log.getChild(type(self).__name__)
        self.__app = app

    @abstractmethod
    def run(self):
        raise NotImplementedError()

    @property
    def width(self) -> int:
        return self.__app.width

    @property
    def height(self) -> int:
        return self.__app.height

    def update(self):
        self.__app.update()

    def sleep(self, seconds: int):
        self.__app.sleep(seconds)

    def percent_pos(self, x: float, y: float) -> Tuple[int, int]:
        """Convert a percentage position to an absolute position."""
        return (round(self.width * x / 100), round(self.height * y / 100))
