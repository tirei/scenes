# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import turtle
from typing import Callable, Optional

import show_element


class Scene(show_element.ShowElement):
    def __init__(
        self,
        log: "logging.Logger",
        app: "application.Application",
        fg_color: str,
        bg_color: str,
    ):
        super().__init__(log, app)
        self.fg = fg_color
        self.bg = bg_color


class _SceneTurtle(turtle.RawTurtle):
    def __init__(self, update: Callable[[], None], *args):
        self.__update = update
        super().__init__(*args)

    def _update(self):
        self.__update()
        super()._update()


class TurtleScene(Scene):
    def __init__(
        self,
        log: "logging.Logger",
        app: "application.Application",
        canvas: "tkinter.Canvas",
        fg_color: str,
        bg_color: str,
    ):
        super().__init__(log, app, fg_color, bg_color)
        self.__screen = turtle.TurtleScreen(canvas)
        self.__screen.bgcolor(bg_color)

    def pen(self, x: int, y: Optional[int] = None, speed=3) -> _SceneTurtle:
        pen = _SceneTurtle(self.update, self.__screen)
        pen.hideturtle()
        pen.color(self.fg)
        pen.penup()
        pen.speed(0)
        pen.setposition(x, y)
        pen.speed(speed)
        pen.pendown()
        pen.showturtle()
        return pen

    def turn_off(self, pen: _SceneTurtle):
        pen.penup()
        pen.speed(0)
        pen.hideturtle()

    def turn_on(self, pen: _SceneTurtle, speed=3):
        pen.pendown()
        pen.speed(speed)
        pen.showturtle()
